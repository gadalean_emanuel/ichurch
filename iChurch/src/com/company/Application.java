package com.company;

import com.company.controller.Controller;
import com.company.controller.ControllerImpl;


import com.company.repository.implementations.*;
import com.company.view.View;
import com.company.view.ViewImpl;

import java.io.IOException;

public class Application {

    public static void main(String[] args) throws IOException {
        ChurchRepository churchRepository = new ChurchRepository();
        DepartmentRepository departmentRepository = new DepartmentRepository();
        MemberRepository memberRepository = new MemberRepository(departmentRepository);
        CaregiverRepository caregiverRepository = new CaregiverRepository(departmentRepository, memberRepository);
        ChurchPersonRepository churchPersonRepository = new ChurchPersonRepository(departmentRepository, memberRepository, caregiverRepository);
        Controller controller = new ControllerImpl(churchPersonRepository, memberRepository, caregiverRepository, departmentRepository, churchRepository);
        View view = new ViewImpl(controller);
        view.start();
    }

}

