package com.company.view;

import java.io.IOException;
import java.text.ParseException;

public interface View {

    void start() throws IOException;

}
