package com.company.view;

import com.company.controller.Controller;
import com.company.model.*;
import com.company.repository.util.*;
import com.company.view.util.UserOption;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static com.company.repository.util.InputOutput.readInt;
import static com.company.repository.util.InputOutput.readString;
import static com.company.repository.util.MenuBuilder.*;

public class ViewImpl implements View {

    private final Controller controller;

    public ViewImpl(Controller controller) {
        this.controller = controller;
    }


    public void start() throws IOException {
        while (true) {
            System.out.print(menu());
            String userMenuOption = readString();
            try {
                Validator.validateInput(userMenuOption);
            } catch (IllegalArgumentException e) {
                System.out.println("Nu exista aceasta optiune in meniu!");
            }
            if (userMenuOption.equals(UserOption.DD.name())) {
                buildDisplayDataMenu();
            } else if (userMenuOption.equals(UserOption.F.name())) {
                buildFiltering();
            } else if (userMenuOption.equals(UserOption.FT.name())) {
                functionalities();
            } else if (userMenuOption.equals(UserOption.EX.name())) {
                System.out.print("""
                        Sunteti sigur ca doriti sa inchideti aplicatia?
                                    DA              NU
                        """);
                String exitOption = readString();
                switch (exitOption) {
                    case "NU":
                        break;
                    case "DA":
                        return;
                }
            }
        }
    }

    private void functionalities() throws IOException {
        while (true) {
            System.out.print(functionalitiesMenu());
            String readUserFunctionality = readString();
            try {
                Validator.validateInput(readUserFunctionality);
            } catch (IllegalArgumentException e) {
                System.out.println("Nu exista aceasta optiune in meniu!");
                break;
            }
            UserOption userFunctionalityOption = UserOption.valueOf(readUserFunctionality);
            switch (userFunctionalityOption) {
                case AM:
                    addMember();
                    break;

                case AC:
                    addCaregiver();
                    break;

                case AR:
                    printChurchPeople();
                    System.out.println("Introduceti id-ul persoanei careia doriti sa adaugati reposnsabilitati!");
                    int personIdInput = readInt();
                    insertResponsibilities(controller.getChurchPersonById(personIdInput));
                    break;

                case AP:
                    printChurchPeople();
                    System.out.println("Introduceti id-ul persoanei pe care doriti sa o adaugati in departament!");
                    int personIdInput1 = readInt();
                    addPersonInADepartment(controller.getChurchPersonById(personIdInput1));
                    break;

                case O:
                    printMembers();
                    System.out.println("Introduceti id-ul persoanei pe care doriti sa ordinati!");
                    int personId = readInt();
                    for (Ordinary ordinary : Ordinary.values()) {
                        System.out.println(ordinary.name());
                    }
                    System.out.println("Introduceti slujba la care va fi ordinat membrul!");
                    String ordinaryInput = readString();
                    ordainsMember(personId, ordinaryInput);
                    break;

                case T:
                    printCareGivers();
                    System.out.println("Introduceti id-ul persoanei care s-a botezat!");
                    int caregiverId = readInt();
                    fromCaregiverToMember(controller.getCaregiverById(caregiverId));
                    break;

                case AG:
                    printDepartments();
                    System.out.println("\nIntroduceti id-ul departamentului in care vreti sa adaugati grupul!");
                    int departmentIdInput = readInt();
                    addGroupInADepartment(departmentIdInput);
                    break;

                case AD:
                    addDepartment();
                    break;

                case DM:
                    printMembers();
                    System.out.println("Introduceti id-ul membrului pe care doriti sa il stergeti!");
                    int id = readInt();
                    deleteMember(controller.getMemberById(id));
                    break;

                case DC:
                    printCareGivers();
                    System.out.println("Intrduceti id-ul apartinatorului pe care doriti sa il stergeti!");
                    int idd = readInt();
                    deleteCaregiver(controller.getCaregiverById(idd));
                    break;

                case EX:
                    return;
            }
        }

    }

    private void buildDisplayDataMenu() throws IOException {
        while (true) {
            System.out.print(displayDataMenu());
            String userDisplayDataOption = readString();
            try {
                Validator.validateInput(userDisplayDataOption);
            } catch (IllegalArgumentException e) {
                System.out.println("Nu exista aceasta optiune in meniu!");
                break;
            }
            UserOption userOption = UserOption.valueOf(userDisplayDataOption);
            switch (userOption) {
                case CGI:
                    printChurchGeneralInfo();
                    System.out.print("""
                            Doriti sa vedeti detalii despre o sala anume din biserica?
                                            DA             NU
                            """);
                    String userQOption = readString();
                    if (userQOption.equals("DA")) {
                        System.out.println("Introduceti id-ul etajului si apoi id-ul salii!");
                        int floorIdOption = readInt();
                        int HallIdOption = readInt();
                        printHalLsData(floorIdOption, HallIdOption);
                    } else {
                        break;
                    }
                    break;

                case M:
                    printMembers();
                    break;

                case A:
                    printCareGivers();
                    break;

                case C:
                    printCommittee();
                    break;

                case P:
                    printPastors();
                    System.out.println();
                    break;

                case O:
                    printOrderlyPeople();
                    break;

                case D:
                    printDepartments();
                    System.out.println();
                    break;

                case CD:
                    System.out.println("Alegeti departament dupa id!");
                    System.out.println("<<<Departamente>>>");
                    for (Department department : controller.getDepartments()) {
                        System.out.println(department.getId() + ": " + department.getName());
                    }
                    String userInputOption = readString();
                    printDepartmentById(Integer.parseInt(userInputOption));
                    break;

                case CR:
                    System.out.println("Alegeti responsabilitate!");
                    for (Responsibilities responsibility : Responsibilities.values()) {
                        if (responsibility.equals(Responsibilities.Comitet)) {
                        } else {
                            System.out.println(responsibility.name());
                        }
                    }
                    String userResponsibilityOption = readString();
                    printPeopleOnAResponsibility(userResponsibilityOption);
                    System.out.println();
                    break;

                case L:
                    printDepartmentsGroupsLeaders();
                    System.out.println();
                    break;

                case EX:
                    return;
            }
        }

    }


    private void printChurchGeneralInfo() throws IOException {
        Church church = controller.getChurchGeneralInfo();
        Map<Integer, List<Integer>> routerMap = controller.getHallFloorRouter().getRouter();
        System.out.println("Numele bisericii: " + church.getName());
        System.out.println("Adresa: " + church.getAddress());
        System.out.println("Data inaugurarii: " + church.getDateOfEstablishment());
        System.out.println("~~~Etaje~~~");
        for (Floor floor : church.getFloors()) {
            System.out.println("        Etajul " + floor.getNumber() + "(id: " + floor.getId() + ")");
            System.out.println("Dimensiune: " + floor.getSquareMeters() + " m^2");
            System.out.println("Are " + floor.getNumberOfHalls() + " sali");
            System.out.print("Sali: ");
            for (int i = 0; i < routerMap.get(floor.getId()).size(); i++) {
                if (i == routerMap.get(floor.getId()).size() - 1) {
                    System.out.print(routerMap.get(floor.getId()).get(i));
                } else {
                    System.out.print(routerMap.get(floor.getId()).get(i) + ",");
                }
            }
            System.out.println();
        }
    }

    private void printHalLsData(int floorIdOption, int hallIdOption) throws IOException {
        List<Integer> hallsIds = controller.getHallFloorRouter().getRouter().get(floorIdOption);
        for (int hallId : hallsIds) {
            if (hallId == hallIdOption) {
                System.out.println(controller.getHallById(hallId));
            }
        }
    }


    /**
     * print all church members
     */
    private void printMembers() throws IOException {
        for (Member member : controller.getMembersList()) {
            System.out.println(member);
        }
        System.out.println();
    }


    /**
     * print all church caregivers
     */
    private void printCareGivers() throws IOException {
        for (Caregiver caregiver : controller.getCaregiversList()) {
            System.out.println(caregiver);
        }
        System.out.println();
    }

    private void printChurchPeople() throws IOException {
        printMembers();
        printCareGivers();
    }

    /**
     * print the church committee
     */
    private void printCommittee() throws IOException {
        List<Member> members = controller.getMembersList();
        for (Member member : members) {
            if (member.getResponsibilityInChurch() != null) {
                for (String responsibility : member.getResponsibilityInChurch()) {
                    if (responsibility.equals(Responsibilities.Comitet.name())) {
                        System.out.println(member);
                    }
                }
            }
        }
        System.out.println();
    }


    /**
     * print the church pastors
     */
    private void printPastors() throws IOException {
        List<Member> members = controller.getMembersList();
        for (Member member : members) {
            if (member.getOrdinary().equals(Ordinary.Pastor.name())) {
                System.out.println("Pastor: " + member.getFullName() + ", CNP: " + member.getPersonalIdentificationNumber() +
                        ", nascut pe: " + member.getBirthday().format(DateFormat.BIRTHDAY_FORMATTER) +
                        ", statut civil: " + member.getMaritalStatus() +
                        ", sex: " + member.getGender() +
                        ", data membralitate: " + member.getMembershipDate().format(DateFormat.BIRTHDAY_FORMATTER) +
                        ", implicat in departamentele: " + member.getDepartmentsInvolvedIn());
            }
            if (member.getOrdinary().equals(Ordinary.Pastor_asistent.name())) {
                System.out.println("Pastor asistent: " + member.getFullName() + ", CNP: " + member.getPersonalIdentificationNumber() +
                        ", nascut pe: " + member.getBirthday().format(DateFormat.BIRTHDAY_FORMATTER) +
                        ", statut civil: " + member.getMaritalStatus() +
                        ", sex: " + member.getGender() +
                        ", data membralitate: " + member.getMembershipDate().format(DateFormat.BIRTHDAY_FORMATTER) +
                        ", implicat in departamentele: " + member.getDepartmentsInvolvedIn());
            }
        }
    }


    /**
     * print ordained ministers in the chruch
     */
    private void printOrderlyPeople() throws IOException {
        List<Member> members = controller.getMembersList();
        for (Member member : members) {
            if (member.getOrdinary().equals(Ordinary.Diacon.name()) || member.getOrdinary().equals(Ordinary.Prezbiter.name())) {
                System.out.println(member);
            }
        }
        System.out.println();
    }


    /**
     * print church departments(here are just listed)
     */
    private void printDepartments() throws IOException {
        List<Department> departments = controller.getDepartments();
        for (Department department : departments) {
            System.out.println("Department" + "(" + department.getId() + "): " + department.getName() + ", leader departament: " + controller.getChurchPersonById(department.getLeaderId()).getFullName());
        }
    }


    /**
     * print info about all groups in a department(chosen by id)
     *
     * @param id - id of a department
     */
    private void printDepartmentById(int id) throws IOException {
        List<DepartmentGroup> groups = controller.getDepartmentsGroups();
        for (DepartmentGroup group : groups) {
            if (group.getDepartmentId() == id) {
                System.out.println("~~~~~" + group.getName() + "~~~~");
                List<GroupMember> groupMembersByGroupId = controller.getGroupById(group.getId());
                for (GroupMember groupMember : groupMembersByGroupId) {
                    System.out.print(controller.getChurchPersonById(groupMember.getPersonId()).getFullName() + "\n" + "Responsabilitati in grup: ");
                    if (groupMember.getRoles().size() == 0) {
                        System.out.println("Nu are responsabiliati!" + "\n");
                    } else {
                        for (int i = 0; i < groupMember.getRoles().size(); i++) {
                            if (i == groupMember.getRoles().size() - 1) {
                                System.out.println(groupMember.getRoles().get(i) + "\n");
                            } else {
                                System.out.print(groupMember.getRoles().get(i) + ", ");
                            }
                        }
                    }
                }
            }
        }
        System.out.println();
    }


    /**
     * print all persons in the church involved in (@param) responsibility
     *
     * @param responsibility - responsibility in church
     */
    private void printPeopleOnAResponsibility(String responsibility) throws IOException {
        List<Member> members = controller.getMembersList();
        List<Caregiver> caregivers = controller.getCaregiversList();
        List<Person> persons = new ArrayList<>();
        persons.addAll(members);
        persons.addAll(caregivers);

        System.out.println(responsibility + ": ");
        for (Person person : persons) {
            for (String personResponsibility : person.getResponsibilitiesInChurch(person)) {
                if (personResponsibility.equals(responsibility)) {
                    System.out.println(person.getFullName());
                }
            }
        }

    }

    /**
     * print all group leaders from all departments
     */
    private void printDepartmentsGroupsLeaders() throws IOException {
        List<GroupMember> groupMembers = controller.getGroupsMembersRoles();
        for (GroupMember groupMember : groupMembers) {
            for (String role : groupMember.getRoles()) {
                if (role.equals("leader")) {
                    System.out.println(controller.getChurchPersonById(groupMember.getPersonId()).getFullName() + "-> leader in " + controller.getGroupNameById(groupMember.getGroupId()));
                }
            }
        }
    }

    /**
     * this method build a list with criteria given by the user
     *
     * @throws IOException - file error
     */
    private void buildFiltering() throws IOException {
        List<String> criteria = new ArrayList<>();
        boolean exit = false;
        System.out.print(filtering());
        while (!exit) {
            String userFilteringOption = readString();
            try {
                Validator.validateInput(userFilteringOption);
            } catch (IllegalArgumentException e) {
                System.out.println("Nu se poate filtra dupa optiunea dumneavoastra! Va rugam reincercati!");
                return;
            }
            if (userFilteringOption.equals(UserOption.EX.name())) {
                exit = true;
            } else {
                criteria.add(userFilteringOption);
            }
        }
        if (criteria.isEmpty()) {
            printChurchPeople();
            return;
        }
        filteringList(criteria);
        System.out.println();

    }

    /**
     * print filtered list by (@param) criteria
     *
     * @param criteria - a list with criteria(String objects);
     */
    private void filteringList(List<String> criteria) throws IOException {
        List<ChurchPerson> filteredList = controller.getChurchPeople();

        for (String criterion : criteria) {
            System.out.println("Introduceti valoarea pentru criteriul " + criterion);
            String criterionValue = readString();
            filteredList = filtration(criterion, criterionValue, filteredList);

        }
        if (filteredList.isEmpty()) {
            System.out.println("Ne pare rau nu am gasit nici o potrivire!");
        } else {
            for (ChurchPerson person : filteredList) {
                System.out.println(person);
            }
        }
    }


    /**
     * @param criterion      - a criterion as a String
     * @param criterionValue - each criterion has a value(e.g for criterion: gender => criterionValue: male)
     * @param filteredList   - a list with persons
     * @return - a list filtered by criterion and criterionValue
     */
    private List<ChurchPerson> filtration(String criterion, String criterionValue, List<ChurchPerson> filteredList) {
        List<ChurchPerson> list = new ArrayList<>();
        for (ChurchPerson person : filteredList) {
            UserOption criterionEnum = UserOption.valueOf(criterion);
            switch (criterionEnum) {
                case Nume:
                    if (person.getFullName().contains(criterionValue)) {
                        list.add(person);
                    }
                    break;
                case Gen:
                    if (person.getGender().contains(criterionValue)) {
                        list.add(person);
                    }
                    break;
                case Varsta:
                    LocalDate date = LocalDate.now();

                    if (date.getYear() - person.getBirthday().getYear() == Integer.parseInt(criterionValue)) {
                        list.add(person);
                    }
                    break;
                case Starea_civila:
                    if (person.getMaritalStatus().contains(criterionValue)) {
                        list.add(person);
                    }

            }
        }
        return list;
    }

    private void addMember() throws IOException {
        System.out.println("Nume:");
        String name = readString();
        System.out.println("CNP:");
        int cnp = readInt();
        System.out.println("Data nasterii:");
        String birthday = readString();
        System.out.println("Statut civil:");
        String maritalStatus = readString();
        System.out.println("Sex:");
        String gender = readString();
        System.out.println("Data membralitate:");
        String membershipDateAsString = readString();

        LocalDate memberBirthday = LocalDate.parse(birthday, DateFormat.BIRTHDAY_FORMATTER);
        LocalDate membershipDate = LocalDate.parse(membershipDateAsString, DateFormat.BIRTHDAY_FORMATTER);

        controller.addMember(name, cnp, memberBirthday, maritalStatus, gender, membershipDate);
        System.out.println("Membrul a fost adaugat cu succes!\n");
    }

    private void addCaregiver() throws IOException {
        System.out.println("Nume:");
        String name = readString();
        System.out.println("CNP:");
        int cnp = readInt();
        System.out.println("Data nasterii:");
        String birthday = readString();
        System.out.println("Statut civil:");
        String maritalStatus = readString();
        System.out.println("Sex:");
        String gender = readString();

        LocalDate caregiverBirthday = LocalDate.parse(birthday, DateFormat.BIRTHDAY_FORMATTER);

        controller.addCaregiver(name, cnp, caregiverBirthday, maritalStatus, gender);
        System.out.println("Apartinatorul a fost adaugat cu succes!\n");
    }

    /**
     * add responsibilities to a church person
     *
     * @param churchPerson - an object(can be either a member or a caregiver)
     * @throws IOException - file error
     */
    private void insertResponsibilities(ChurchPerson churchPerson) throws IOException {
        System.out.println("Puteti alege din urmatoarele responsabilitati!");
        for (Responsibilities responsibilities : Responsibilities.values()) {
            System.out.println(responsibilities.name());
        }
        System.out.println("Puteti adauga oricate responsabilitati doriti! Dupa ce ati terminat va rugam introduceti exit!");
        List<String> responsibilities = new ArrayList<>();
        do {
            String responsibilityUserOption = readString();
            if (responsibilityUserOption.equals("exit")) {
                break;
            } else {
                responsibilities.add(responsibilityUserOption);
            }
        } while (true);

        controller.insertResponsibilitiesToAChurchPerson(churchPerson, responsibilities);
        System.out.println("Responsabilitatile au fost adaugate cu succes!" + "\n");
    }

    /**
     * add a person in a group of a department
     *
     * @param churchPerson - an object(can be either a member or a caregiver)
     * @throws IOException - file error
     */
    private void addPersonInADepartment(ChurchPerson churchPerson) throws IOException {
        System.out.println("Alegeti departamentul(dupa id)");
        for (Department department : controller.getDepartments()) {
            System.out.println(department);
        }
        int userDepartment = readInt();

        List<DepartmentGroup> departmentGroups = controller.getDepartmentsGroups();
        System.out.println("Alegeti grupa din departament in care doreste sa fie implicat(dupa id)!");
        for (DepartmentGroup departmentGroup : departmentGroups) {
            if (departmentGroup.getDepartmentId() == userDepartment) {
                System.out.println(departmentGroup.getName() + "(id:" + departmentGroup.getId() + ")");
            }
        }
        int userDepartmentGroupOption = readInt();

        System.out.println("Introduceti responsabilitatile pe care o sa le aiba in trupa! Cand ati terminat introduceti exit!");
        List<String> responsibilities = new ArrayList<>();
        do {
            String userResponsibilityOption = readString();
            if (userResponsibilityOption.equals("exit")) {
                break;
            } else {
                responsibilities.add(userResponsibilityOption);
            }

        } while (true);
        controller.addPersonInADepartmentGroup(churchPerson, userDepartmentGroupOption, responsibilities);
        System.out.println("Persoana a fost adaugata cu succes!\n");
    }

    private void ordainsMember(int id, String ordinary) throws IOException {
        controller.ordainsMember(controller.getMemberById(id), ordinary);
        System.out.println("Membru a fost ordinat cu succes!\n");
    }

    /**
     * this method transforms a caregiver object into a member
     *
     * @param caregiver - a caregiver
     * @throws IOException - file error
     */
    private void fromCaregiverToMember(Caregiver caregiver) throws IOException {
        System.out.println("Introduceti data botezului!");
        String baptizeDateAsString = readString();
        LocalDate baptizeDate = LocalDate.parse(baptizeDateAsString, DateTimeFormatter.ofPattern(DateFormat.DATE_FORMAT));
        controller.fromCaregiverToMember(caregiver, baptizeDate);

        System.out.println("Transferul pe lista membrilor s-a efectuat cu succes!\n");
    }

    private void addGroupInADepartment(int departmentId) throws IOException {
        System.out.println("Introduceti numele noului grup!");
        String groupName = readString();
        controller.addGroupInADepartment(departmentId, groupName);
        System.out.println("Grupul a fost adaugat cu succes!\n");
    }

    private void addDepartment() throws IOException {
        System.out.println("Introduceti numele departamentului!");
        String departmentNameInput = readString();
        printMembers();
        System.out.println("Introduceti id-ul persoanei care va fi leaderul noului departament!");
        int leaderIdInput = readInt();
        controller.addDepartment(departmentNameInput, leaderIdInput);
        System.out.println("Departamentul a fost adaugat cu succes!\nE nevoie sa adaugati o grupa in departament!");
        addGroupInADepartment(controller.getDepartments().get(controller.getDepartments().size() - 1).getId());
    }

    private void deleteMember(Member member) throws IOException {
        controller.deleteMember(member);
        System.out.println("Membrul a fost sters cu succes!\n");
    }

    private void deleteCaregiver(Caregiver caregiver) throws IOException {
        controller.deleteCaregiver(caregiver);
        System.out.println("Apartinatorul a fost sters cu succes!\n");
    }

}
