package com.company.controller;

import com.company.model.*;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

public interface Controller {

    List<Member> getMembersList() throws IOException;

    List<Caregiver> getCaregiversList() throws IOException;

    List<Department> getDepartments() throws IOException;

    Church getChurchGeneralInfo() throws IOException;

    List<GroupMember> getGroupsMembersRoles() throws IOException;

    ChurchPerson getChurchPersonById(int id) throws IOException;

    List<ChurchPerson> getChurchPeople() throws IOException;

    Member getMemberById(int id) throws IOException;

    Caregiver getCaregiverById(int id) throws IOException;

    List<DepartmentGroup> getDepartmentsGroups() throws IOException;

    List<GroupMember> getGroupById(int groupId) throws IOException;

    void addMember(String name, int cnp, LocalDate birthday, String maritalStatus, String gender, LocalDate membershipDate) throws IOException;

    void insertResponsibilitiesToAChurchPerson(ChurchPerson churchPerson, List<String> responsibilities) throws IOException;

    void addPersonInADepartmentGroup(ChurchPerson churchPerson, int userDepartmentGroupOption, List<String> responsibilities) throws IOException;

    void addCaregiver(String name, int cnp, LocalDate birthday, String maritalStatus, String gender) throws IOException;

    Router getHallFloorRouter() throws IOException;

    Hall getHallById(int hallId) throws IOException;

    void ordainsMember(Member member, String ordinary) throws IOException;

    void fromCaregiverToMember(Caregiver caregiver, LocalDate baptizeDate) throws IOException;

    String getGroupNameById(int id) throws IOException;

    void addGroupInADepartment(int departmentId, String groupName) throws IOException;

    void addDepartment(String departmentName, int leaderId) throws IOException;

    void deleteMember(Member member) throws IOException;

    void deleteCaregiver(Caregiver caregiver) throws IOException;

}
