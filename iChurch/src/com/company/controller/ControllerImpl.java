package com.company.controller;

import com.company.model.*;
import com.company.repository.implementations.*;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ControllerImpl implements Controller {

    private final ChurchPersonRepository churchPersonRepository;

    private final MemberRepository memberRepository;

    private final CaregiverRepository caregiverRepository;

    private final DepartmentRepository departmentRepository;

    private final ChurchRepository churchRepository;

    public ControllerImpl(ChurchPersonRepository churchPersonRepository, MemberRepository memberRepository, CaregiverRepository caregiverRepository,
                          DepartmentRepository departmentRepository, ChurchRepository churchRepository) {
        this.churchPersonRepository = churchPersonRepository;
        this.memberRepository = memberRepository;
        this.caregiverRepository = caregiverRepository;
        this.departmentRepository = departmentRepository;
        this.churchRepository = churchRepository;
    }

    public List<Member> getMembersList() throws IOException {
        return memberRepository.findAll();
    }

    public List<Caregiver> getCaregiversList() throws IOException {
        return caregiverRepository.findAll();
    }

    public List<Department> getDepartments() throws IOException {
        return departmentRepository.findAll();
    }

    public Church getChurchGeneralInfo() throws IOException {
        return churchRepository.find();
    }

    public List<GroupMember> getGroupsMembersRoles() throws IOException {
        return departmentRepository.findAllGroupMembers();
    }

    public ChurchPerson getChurchPersonById(int id) throws IOException {
        return churchPersonRepository.findById(id);
    }

    public Member getMemberById(int id) throws IOException {
        return memberRepository.findById(id);
    }

    public Caregiver getCaregiverById(int id) throws IOException {
        return caregiverRepository.findById(id);
    }

    public List<DepartmentGroup> getDepartmentsGroups() throws IOException {
        return departmentRepository.findAllGroupsFromAllDepartments();
    }

    public List<GroupMember> getGroupById(int id) throws IOException {
        return departmentRepository.findGroupMembersByGroupId(id);
    }

    public void addMember(String name, int cnp, LocalDate birthday, String maritalStatus, String gender, LocalDate membershipDate) throws IOException {
        Member newMember = new Member(name, cnp, birthday, maritalStatus, gender, membershipDate, new ArrayList<>(), new ArrayList<>());
        newMember.setOrdinary("Neordinat");
        memberRepository.insert(newMember);
    }

    public void insertResponsibilitiesToAChurchPerson(ChurchPerson churchPerson, List<String> responsibilities) throws IOException {
        List<String> currentResponsibilities = churchPerson.getResponsibilityInChurch();
        currentResponsibilities.addAll(responsibilities);
        churchPerson.setResponsibilityInChurch(currentResponsibilities);
        churchPersonRepository.insertResponsibilities(churchPerson);
    }

    public void addPersonInADepartmentGroup(ChurchPerson churchPerson, int groupId, List<String> responsibilities) throws IOException {
        GroupMember groupMember = new GroupMember(churchPerson.getId(), responsibilities, groupId);
        churchPersonRepository.insertPersonInADepartmentGroup(groupMember);
    }

    public void addCaregiver(String name, int cnp, LocalDate birthday, String maritalStatus, String gender) throws IOException {
        Caregiver caregiver = new Caregiver(name, cnp, birthday, maritalStatus, gender);
        caregiverRepository.insert(caregiver);
    }

    public Router getHallFloorRouter() throws IOException {
        return churchRepository.findHallFloorRouter();
    }

    public Hall getHallById(int hallId) throws IOException {
        return churchRepository.findHallById(hallId);
    }

    public void ordainsMember(Member member, String ordinary) throws IOException {
        member.setOrdinary(ordinary);
        memberRepository.update(member);
    }

    public void fromCaregiverToMember(Caregiver caregiver, LocalDate baptizeDate) throws IOException {
        Member newMember = new Member(caregiver.getFullName(), caregiver.getPersonalIdentificationNumber(), caregiver.getBirthday(),
                caregiver.getMaritalStatus(), caregiver.getGender(), baptizeDate, new ArrayList<>(), new ArrayList<>());
        caregiverRepository.fromCaregiverToMember(caregiver, newMember);
    }

    public String getGroupNameById(int id) throws IOException {
        return departmentRepository.findGroupNameById(id);
    }

    public void addGroupInADepartment(int departmentId, String groupName) throws IOException {
        departmentRepository.insertGroupInADepartment(departmentId, groupName);
    }

    public void addDepartment(String departmentName, int leaderId) throws IOException {
        Department newDepartment = new Department(departmentName, leaderId);
        departmentRepository.insert(newDepartment);
    }

    public void deleteMember(Member member) throws IOException {
        memberRepository.delete(member);
    }

    public void deleteCaregiver(Caregiver caregiver) throws IOException {
        caregiverRepository.delete(caregiver);
    }

    public List<ChurchPerson> getChurchPeople() throws IOException {
        return churchPersonRepository.findAll();
    }

}
