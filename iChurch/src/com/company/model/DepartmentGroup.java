package com.company.model;

/**
 * this class represents a group in a department
 * any department should have groups(one or more)
 */
public class DepartmentGroup extends Entity {

    private String name;

    private int departmentId;

    public DepartmentGroup(int id, String name, int departmentId) {
        setId(id);
        this.name = name;
        this.departmentId = departmentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }
}

