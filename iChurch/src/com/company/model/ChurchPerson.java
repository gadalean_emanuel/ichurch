package com.company.model;

import java.util.List;

/**
 * class contains:
 * -responsibilities of a person in the church
 * -departments in which they are involved
 */
public class ChurchPerson extends Person implements InFileEntity {

    private List<String> responsibilityInChurch;

    private List<String> departmentsInvolvedIn;

    public List<String> getResponsibilityInChurch() {
        return responsibilityInChurch;
    }

    public void setResponsibilityInChurch(List<String> responsibilityInChurch) {
        this.responsibilityInChurch = responsibilityInChurch;
    }

    public List<String> getDepartmentsInvolvedIn() {
        return departmentsInvolvedIn;
    }

    public void setDepartmentsInvolvedIn(List<String> departmentsInvolvedIn) {
        this.departmentsInvolvedIn = departmentsInvolvedIn;
    }

    @Override
    public void setData(String... data) {

    }

    @Override
    public String getData() {
        String responsibilitiesReady = "";
        List<String> memberResponsibilities = getResponsibilityInChurch();
        for (int i = 0; i < memberResponsibilities.size(); i++) {
            if (i == memberResponsibilities.size() - 1) {
                responsibilitiesReady += memberResponsibilities.get(i);
            } else {
                responsibilitiesReady += memberResponsibilities.get(i) + ",";
            }
        }
        return getId() + "," + responsibilitiesReady;

    }
}
