package com.company.model;

public class Department extends Entity implements InFileEntity {

    private String name;

    private int leaderId;

    public Department(String name, int leaderId) {
        this.name = name;
        this.leaderId = leaderId;
    }

    public Department() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLeaderId() {
        return leaderId;
    }

    public void setLeaderId(int leaderId) {
        this.leaderId = leaderId;
    }

    @Override
    public String toString() {
        return "Department: " +
                name + "(" + super.toString() + ")";
    }

    @Override
    public void setData(String... data) {
        setId(Integer.parseInt(data[0]));
        name = data[1];
        leaderId = Integer.parseInt(data[2].trim());
    }

    @Override
    public String getData() {
        return getId() + "," + getName() + "," + getLeaderId() + "\n";
    }

}
