package com.company.model;

import com.company.repository.util.DateFormat;

import java.time.LocalDate;
import java.util.Objects;

public class Caregiver extends ChurchPerson implements InFileEntity {

    public Caregiver(String fullName, int personalIdentificationNumber, LocalDate birthday, String maritalStatus, String gender) {
        setFullName(fullName);
        setPersonalIdentificationNumber(personalIdentificationNumber);
        setBirthday(birthday);
        setMaritalStatus(maritalStatus);
        setGender(gender);
    }

    public Caregiver() {

    }

    @Override
    public String toString() {
        return "Apartinator(id:" +
                super.toString() +
                ", responsibilitati: " + getResponsibilityInChurch() +
                ", implicate in departamentele: " + getDepartmentsInvolvedIn();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Caregiver caregiver = (Caregiver) obj;
        return Objects.equals(getId(), caregiver.getId());

    }

    @Override
    public void setData(String... data) {
        setId(Integer.parseInt(data[0]));
        setFullName(data[1]);
        setPersonalIdentificationNumber(Integer.parseInt(data[2]));
        setBirthday(LocalDate.parse(data[3], DateFormat.BIRTHDAY_FORMATTER));
        setMaritalStatus(data[4]);
        setGender(data[5].trim());
    }

    @Override
    public String getData() {
        return getId() + "," + getFullName() + "," + getPersonalIdentificationNumber()
                + "," + DateFormat.BIRTHDAY_FORMATTER.format(getBirthday()) + "," + getMaritalStatus() + "," + getGender() + "\n";
    }
}
