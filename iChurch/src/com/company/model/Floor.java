package com.company.model;

public class Floor extends Entity{

    private int number;

    private int squareMeters;

    private int numberOfHalls;

    public Floor(int id,int number, int squareMeters, int numberOfHalls) {
        setId(id);
        this.number = number;
        this.squareMeters = squareMeters;
        this.numberOfHalls = numberOfHalls;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getSquareMeters() {
        return squareMeters;
    }

    public void setSquareMeters(int squareMeters) {
        this.squareMeters = squareMeters;
    }

    public int getNumberOfHalls() {
        return numberOfHalls;
    }

    public void setNumberOfHalls(int numberOfHalls) {
        this.numberOfHalls = numberOfHalls;
    }

    @Override
    public String toString() {
        return "Floor{" +
                "number=" + number +
                ", squareMeters=" + squareMeters +
                ", numberOfHalls=" + numberOfHalls +
                "} " + super.toString();
    }
}
