package com.company.model;

import java.util.List;

/**
 * class contains roles of the group member
 */
public class GroupMember {

    private int personId;

    private List<String> roles;

    private int groupId;

    public GroupMember(int personId, List<String> roles,int groupId) {
        this.personId = personId;
        this.roles = roles;
        this.groupId = groupId;
    }

    public GroupMember(){};

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }
}
