package com.company.model;

public class Hall extends Entity{

    private int squareMeters;

    private int capacity;

    private String usage;

    public Hall(int id,int squareMeters, int capacity, String usage) {
        setId(id);
        this.squareMeters = squareMeters;
        this.capacity = capacity;
        this.usage = usage;
    }

    public int getSquareMeters() {
        return squareMeters;
    }

    public void setSquareMeters(int squareMeters) {
        this.squareMeters = squareMeters;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }


    @Override
    public String toString() {
        return "Sala " + super.toString() +
                 ": " + squareMeters + " m^2, " +
                "capacitate= " + capacity +
                " de persoane, utilizari= " + usage;
    }
}
