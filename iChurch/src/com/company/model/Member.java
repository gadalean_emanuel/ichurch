package com.company.model;

import com.company.repository.util.DateFormat;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

public class Member extends ChurchPerson implements InFileEntity {

    private LocalDate membershipDate;

    private String ordinary;

    public Member(String fullName, int personalIdentificationNumber, LocalDate birthday, String maritalStatus, String gender, LocalDate membershipDate, List<String> responsibilityInChurch, List<String> departmentsInvolvedIn) {
        setFullName(fullName);
        setPersonalIdentificationNumber(personalIdentificationNumber);
        setBirthday(birthday);
        setMaritalStatus(maritalStatus);
        setGender(gender);
        this.membershipDate = membershipDate;
        setResponsibilityInChurch(responsibilityInChurch);
        setDepartmentsInvolvedIn(departmentsInvolvedIn);
    }

    public Member(int id,String fullName, int personalIdentificationNumber, LocalDate birthday, String maritalStatus, String gender, LocalDate membershipDate,String ordinary) {
        setId(id);
        setFullName(fullName);
        setPersonalIdentificationNumber(personalIdentificationNumber);
        setBirthday(birthday);
        setMaritalStatus(maritalStatus);
        setGender(gender);
        this.membershipDate = membershipDate;
        this.ordinary = ordinary;
    }

    public Member() {

    }

    public LocalDate getMembershipDate() {
        return membershipDate;
    }

    public void setMembershipDate(LocalDate membershipDate) {
        this.membershipDate = membershipDate;
    }

    public String getOrdinary() {
        return ordinary;
    }

    public void setOrdinary(String ordinary) {
        this.ordinary = ordinary;
    }

    @Override
    public String toString() {
        return "Member(id:" +
                super.toString() +
                ", data membralitate: " + membershipDate.format(DateFormat.BIRTHDAY_FORMATTER) +
                ", responsabilitati: " + getResponsibilityInChurch() +
                ", ordinnare: " + ordinary +
                ", implicat in departamentele: " + getDepartmentsInvolvedIn();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Member member = (Member) o;
        return Objects.equals(membershipDate, member.membershipDate) && Objects.equals(ordinary, member.ordinary);
    }

    @Override
    public void setData(String... data) {
        setId(Integer.parseInt(data[0]));
        setFullName(data[1]);
        setPersonalIdentificationNumber(Integer.parseInt(data[2]));
        setBirthday(LocalDate.parse(data[3], DateFormat.BIRTHDAY_FORMATTER));
        setMaritalStatus(data[4]);
        setGender(data[5]);
        setMembershipDate(LocalDate.parse(data[6], DateFormat.BIRTHDAY_FORMATTER));
        setOrdinary(data[7].trim());
    }

    @Override
    public String getData() {
        return getId() + "," + getFullName() + "," + getPersonalIdentificationNumber()
                + "," + DateFormat.BIRTHDAY_FORMATTER.format(getBirthday()) + "," + getMaritalStatus() + "," + getGender() + "," +
                DateFormat.BIRTHDAY_FORMATTER.format(getMembershipDate()) + "," + getOrdinary() + "\n";
    }
}
