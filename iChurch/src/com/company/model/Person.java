package com.company.model;

import com.company.repository.util.DateFormat;

import java.time.LocalDate;
import java.util.List;

public abstract class Person extends Entity {

    private String fullName;

    private Integer personalIdentificationNumber;

    private LocalDate birthday;

    private String maritalStatus;

    private String gender;


    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getPersonalIdentificationNumber() {
        return personalIdentificationNumber;
    }

    public void setPersonalIdentificationNumber(Integer personalIdentificationNumber) {
        this.personalIdentificationNumber = personalIdentificationNumber;
    }

    public List<String> getResponsibilitiesInChurch(Object o) {
        if (o.getClass() == Member.class) {
            return ((Member) o).getResponsibilityInChurch();
        } else {
            return ((Caregiver) o).getResponsibilityInChurch();
        }
    }

    @Override
    public String toString() {
        return getId() + ")" + fullName + ", CNP= " + personalIdentificationNumber +
                ", nascut pe: " + birthday.format(DateFormat.BIRTHDAY_FORMATTER) +
                ", statut civil: " + maritalStatus +
                ", sex: " + gender;
    }
}
