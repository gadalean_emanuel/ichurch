package com.company.model;

public interface InFileEntity {

    void setData(String... data);

    String getData();

}
