package com.company.model;

import java.util.List;
import java.util.Map;

/**
 * class link Floor and Hall
 * key map is an id of a floor
 * map value is a list that contains id of the halls present on that floor
 */
public class Router {

    private Map<Integer, List<Integer>> router;

    public Map<Integer, List<Integer>> getRouter() {
        return router;
    }

    public void setRouter(Map<Integer, List<Integer>> router) {
        this.router = router;
    }

}
