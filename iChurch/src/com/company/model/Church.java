package com.company.model;

import java.util.List;

public class Church extends Entity implements InFileEntity {

    private String name;

    private String address;

    private String dateOfEstablishment;

    private Router hallFloorRouter;

    private List<Floor> floors;

    private List<Hall> halls;

    private List<Person> persons;

    private List<Department> departments;

    private List<DepartmentGroup> departmentGroups;

    private List<GroupMember> groupMembers;

    public Church(String name, String address, String dateOfEstablishment) {
        this.name = name;
        this.address = address;
        this.dateOfEstablishment = dateOfEstablishment;
    }

    public Church() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDateOfEstablishment() {
        return dateOfEstablishment;
    }

    public void setDateOfEstablishment(String dateOfEstablishment) {
        this.dateOfEstablishment = dateOfEstablishment;
    }

    public Router getHallFloorRouter() {
        return hallFloorRouter;
    }

    public void setHallFloorRouter(Router hallFloorRouter) {
        this.hallFloorRouter = hallFloorRouter;
    }


    public List<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(List<Department> departments) {
        this.departments = departments;
    }

    public List<DepartmentGroup> getDepartmentGroups() {
        return departmentGroups;
    }

    public void setDepartmentGroups(List<DepartmentGroup> departmentGroups) {
        this.departmentGroups = departmentGroups;
    }

    public List<GroupMember> getGroupMembers() {
        return groupMembers;
    }

    public void setGroupMembers(List<GroupMember> groupMembers) {
        this.groupMembers = groupMembers;
    }

    public List<Floor> getFloors() {
        return floors;
    }

    public void setFloors(List<Floor> floors) {
        this.floors = floors;
    }

    public List<Hall> getHalls() {
        return halls;
    }

    public void setHalls(List<Hall> halls) {
        this.halls = halls;
    }

    public List<Person> getPersons() {
        return persons;
    }

    public void setPersons(List<Person> persons) {
        this.persons = persons;
    }

    @Override
    public void setData(String... data) {

    }


    @Override
    public String getData() {
        return null;

    }

}
