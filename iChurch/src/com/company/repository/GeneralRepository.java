package com.company.repository;

import com.company.repository.util.Separator;
import com.company.model.Entity;
import com.company.model.InFileEntity;
import com.company.repository.util.FileService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public abstract class GeneralRepository<E extends Entity & InFileEntity> implements Repository<E> {

    private final Supplier<? extends E> instanceSupplier;///provides an instance of a class

    protected abstract Path getPath();///used for writing or reading from file

    protected abstract int generateId();///used in adding an entity

    public GeneralRepository(Supplier<? extends E> instanceSupplier) {
        this.instanceSupplier = instanceSupplier;
    }

    @Override
    public List<E> findAll() throws IOException {
        String fileContent = Files.readString(getPath());
        String[] lines = fileContent.split(Separator.END_LINE);

        List<E> entities = new ArrayList<>();
        for (String line : lines) {
            String[] data = line.split(Separator.COMMA);
            E entity = instanceSupplier.get();
            entity.setData(data);
            entities.add(entity);
        }
        return entities;
    }

    @Override
    public boolean delete(E value) throws IOException {
        List<E> entities = findAll();
        boolean isRemoved = entities.remove(value);
        updateFileContent(entities);
        return isRemoved;
    }

    @Override
    public boolean insert(E entity) throws IOException {
        entity.setId(generateId());
        Files.write(getPath(), entity.getData().getBytes(), StandardOpenOption.APPEND);
        return true;
    }

    @Override
    public E findById(int id) throws IOException {
        for (E entity : findAll()) {
            if (entity.getId() == id) {
                return entity;
            }
        }
        return null;
    }


    @Override
    public void update(E entity) throws IOException {
        List<E> entities = findAll();
        for (int i = 0; i < entities.size(); i++) {
            if (entities.get(i).getId() == entity.getId()) {
                entities.set(i, entity);
                break;
            }
        }
        updateFileContent(entities);
    }

    private void updateFileContent(List<E> entities) throws IOException {
        FileService.deleteFileContent(getPath());
        Files.write(getPath(), entityListReady(entities).getBytes());
    }

    /**
     * @param entities - list with entities(members,caregivers, departments,..)
     * @return - a String structured in the form of csv
     * this method is protected because can be overridden, if something more specific is needed
     */
    protected String entityListReady(List<E> entities) {
        String dataReady = "";
        for (E entity : entities) {
            dataReady += entity.getData();
        }
        return dataReady;
    }

}
