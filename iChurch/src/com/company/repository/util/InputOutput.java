package com.company.repository.util;

import java.util.Scanner;

public class InputOutput {

    public static String readString(){
        Scanner scan = new Scanner(System.in);
        return scan.nextLine();
    }

    public static int readInt(){
        Scanner scan = new Scanner(System.in);
        return scan.nextInt();
    }
}
