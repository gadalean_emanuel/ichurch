package com.company.repository.util;

import com.company.view.util.UserOption;

public class Validator {

    public static void validateInput(String input) throws IllegalArgumentException {
        boolean foundIt = false;
        for (UserOption predefinedValue : UserOption.values()) {
            if (predefinedValue.name().equals(input)) {
                foundIt = true;
                break;
            }
        }

        if (!foundIt) {
            throw new IllegalArgumentException();
        }
    }
}
