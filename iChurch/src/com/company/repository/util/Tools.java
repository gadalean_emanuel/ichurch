package com.company.repository.util;

public class Tools {

    public static int getPositionOfAnObjectInAStringArray(int id, String[] lines) {
        int memoPos = -1;
        for (int pos = 0; pos < lines.length; pos++) {
            String[] responsibilitiesDataTest = lines[pos].split(Separator.COMMA);
            if (responsibilitiesDataTest[0].equals(id + "")) {
                memoPos = pos;
            }
        }
        return memoPos;
    }


}
