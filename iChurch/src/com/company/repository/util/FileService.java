package com.company.repository.util;

import com.company.model.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;


public class FileService {

    public static String personResponsibilityReady(ChurchPerson churchPerson) {
        String responsibilitiesReady = "";
        List<String> memberResponsibilities = churchPerson.getResponsibilityInChurch();
        for (int i = 0; i < memberResponsibilities.size(); i++) {
            if (i == memberResponsibilities.size() - 1) {
                responsibilitiesReady += memberResponsibilities.get(i);
            } else {
                responsibilitiesReady += memberResponsibilities.get(i) + ",";
            }
        }
        return churchPerson.getId() + "," + responsibilitiesReady + "\n";

    }

    public static String updateGroupsFile(List<GroupMember> groupMembers) {
        String fileContent = "";
        for (GroupMember groupMember : groupMembers) {
            fileContent += groupMember.getGroupId() + "," + groupMember.getPersonId() + ",";
            for (int i = 0; i < groupMember.getRoles().size(); i++) {
                if (i == groupMember.getRoles().size() - 1) {
                    fileContent += groupMember.getRoles().get(i) + "\n";
                } else {
                    fileContent += groupMember.getRoles().get(i) + ",";
                }
            }
            if (groupMember.getRoles().isEmpty()) {
                fileContent += "\n";
            }
        }
        return fileContent;
    }

    public static String updateIdInGroupFile(int id, List<Member> members, List<GroupMember> groupMembers) {
        String fileContent = "";
        for (GroupMember groupMember : groupMembers) {
            fileContent += groupMember.getGroupId() + ",";
            if (groupMember.getPersonId() == id) {
                fileContent += members.get(members.size() - 1).getId() + ",";
            } else {
                fileContent += groupMember.getPersonId() + ",";
            }
            for (int i = 0; i < groupMember.getRoles().size(); i++) {
                if (i == groupMember.getRoles().size() - 1) {
                    fileContent += groupMember.getRoles().get(i) + "\n";
                } else {
                    fileContent += groupMember.getRoles().get(i) + ",";
                }
            }
            if (groupMember.getRoles().isEmpty()) {
                fileContent += "\n";
            }
        }
        return fileContent;
    }

    public static String addNewGroupInDepartmentReady(DepartmentGroup departmentGroup) {
        return departmentGroup.getDepartmentId() + "," + departmentGroup.getId() + "," + departmentGroup.getName();
    }

    public static void deleteFileContent(Path path) throws IOException {
        Files.deleteIfExists(path);
        Files.createFile(path);
    }

}
