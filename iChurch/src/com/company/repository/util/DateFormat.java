package com.company.repository.util;

import java.time.format.DateTimeFormatter;

public class DateFormat {

    public static final String DATE_FORMAT = "dd.MM.yyyy";
    public static final DateTimeFormatter BIRTHDAY_FORMATTER = DateTimeFormatter.ofPattern(DateFormat.DATE_FORMAT);
}
