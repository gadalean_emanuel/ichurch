package com.company.repository.util;

public class MenuBuilder {

    public static String menu() {
        return """
                ~~~~Menu~~~~
                DD: Afiseaza date!
                F: Filtrari!
                FT: Functionalitati!
                EX: Inchide aplicatia!
                """;
    }

    public static String displayDataMenu() {
        return """
                CGI: Afiseaza date generale despre biserica!
                M: Afiseaza Membrii!
                A: Afiseaza apartinatorii!
                C: Afiseaza Comitet!
                P: Afiseaza pastorii!
                O: Afiseaza slujitorii ordinati!
                D: Afiseaza departamente!
                CD: Afiseaza departament la alegere!
                CR: Afiseaza oamenii pe o responsabilitate!
                L: Afiseaza toti liderii din orice departament!
                EX: Inapoi la meniul principal!
                """;
    }

    public static String filtering() {
        return """
                Nume: Filtreaza dupa nume!
                Gen: Filtreaza dupa gen!
                Varsta: Filtreaza dupa varsta!
                Starea_civila: Filtreaza dupa starea civila! 
                EX: Stop!
                Introduceti unu sau mai multe criterii dupa care vreti sa filtrati, apoi apasati butonul EX!
                """;
    }

    public static String functionalitiesMenu() {
        return """
                AM: Adauga membru!
                AC: Adauga apartinator!
                AR: Adauga responsabilitati unei persoane!
                AP: Adauga persoana intr-un departament!
                O: Ordineaza membru!
                T: Trecere de la apartinator la membru prin botez!
                AG: Adauga grup intr-un departament!
                AD: Adauga departament!
                DM: Sterge membru!
                DC: Sterge apartinator!
                EX: Inapoi la meniul principal!
                    """;
    }
}
