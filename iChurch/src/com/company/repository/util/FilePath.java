package com.company.repository.util;

import java.nio.file.Path;
import java.nio.file.Paths;

public class FilePath {

    public static final Path MEMBERS_PATH = Paths.get("C:\\Users\\Emanuel Gadalean\\IdeaProjects\\iChurch3\\filerepo\\members&caregivers", "members.txt");
    public static final Path CAREGIVERS_PATH = Paths.get("C:\\Users\\Emanuel Gadalean\\IdeaProjects\\iChurch3\\filerepo\\members&caregivers", "caregivers.txt");
    public static final Path RESPONSIBILITIES_PATH = Paths.get("C:\\Users\\Emanuel Gadalean\\IdeaProjects\\iChurch3\\filerepo\\members&caregivers", "responsibilities.txt");
    public static final Path DEPARTMENTS_PATH = Paths.get("C:\\Users\\Emanuel Gadalean\\IdeaProjects\\iChurch3\\filerepo\\departements", "departements.txt");
    public static final Path CHURCH_GENERAL_INFO = Paths.get("C:\\Users\\Emanuel Gadalean\\IdeaProjects\\iChurch3\\filerepo\\church_general_info", "church.txt");
    public static final Path FLOORS = Paths.get("C:\\Users\\Emanuel Gadalean\\IdeaProjects\\iChurch3\\filerepo\\church_general_info", "church_floors.txt");
    public static final Path HALLS = Paths.get("C:\\Users\\Emanuel Gadalean\\IdeaProjects\\iChurch3\\filerepo\\church_general_info", "church_halls.txt");
    public static final Path DEPARTMENTS_DETAILS = Paths.get("C:\\Users\\Emanuel Gadalean\\IdeaProjects\\iChurch3\\filerepo\\departements", "groups.txt");
    public static final Path GROUPS = Paths.get("C:\\Users\\Emanuel Gadalean\\IdeaProjects\\iChurch3\\filerepo\\departements", "group_members.txt");
}



