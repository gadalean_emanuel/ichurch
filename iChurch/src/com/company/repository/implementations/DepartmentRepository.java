package com.company.repository.implementations;

import com.company.repository.util.Separator;
import com.company.model.Department;
import com.company.model.DepartmentGroup;
import com.company.model.GroupMember;
import com.company.repository.GeneralRepository;
import com.company.repository.util.FileService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

import static com.company.repository.util.FilePath.*;

public class DepartmentRepository extends GeneralRepository<Department> {

    private int departmentId;

    public DepartmentRepository() throws IOException {
        super(Department::new);
        departmentId = findAll().get(findAll().size() - 1).getId();
    }

    public void insertGroupInADepartment(int departmentId, String groupName) throws IOException {
        List<DepartmentGroup> departmentGroups = findAllGroupsFromAllDepartments();
        int newGroupId = departmentGroups.size() + 1;
        DepartmentGroup newGroup = new DepartmentGroup(newGroupId, groupName, departmentId);

        Files.write(DEPARTMENTS_DETAILS, (FileService.addNewGroupInDepartmentReady(newGroup) + "\n").getBytes(), StandardOpenOption.APPEND);
    }


    public String findGroupNameById(int id) throws IOException {
        List<DepartmentGroup> departmentGroups = findAllGroupsFromAllDepartments();
        for (DepartmentGroup departmentGroup : departmentGroups) {
            if (departmentGroup.getId() == id) {
                return departmentGroup.getName();
            }
        }
        return null;
    }

    public List<DepartmentGroup> findAllGroupsFromAllDepartments() throws IOException {
        String fileContent = Files.readString(DEPARTMENTS_DETAILS);
        String[] lines = fileContent.split(Separator.END_LINE);

        List<DepartmentGroup> departmentGroups = new ArrayList<>();
        for (String line : lines) {
            String[] departmentGroup = line.split(Separator.COMMA);
            DepartmentGroup group = new DepartmentGroup(Integer.parseInt(departmentGroup[1]), departmentGroup[2], Integer.parseInt(departmentGroup[0]));
            departmentGroups.add(group);
        }
        return departmentGroups;
    }

    private String getDepartmentByGroupId(int groupId) throws IOException {
        List<DepartmentGroup> departmentGroups = findAllGroupsFromAllDepartments();
        for (DepartmentGroup departmentGroup : departmentGroups) {
            if (departmentGroup.getId() == groupId) {
                // return getDepartmentById(departmentGroup.getDepartmentId()).getName();
                return findById(departmentGroup.getDepartmentId()).getName();
            }
        }
        return null;
    }

    /**
     * findDepartmentsNamesInWhichChurchPersonIsInvolved(int churchPersonId)
     * findDepartmentsNamesForChurchPersonId(int churchPersonId)
     *
     * @param churchPersonId-
     * @return
     * @throws IOException-
     */
    public List<String> findDepartmentsNamesForChurchPersonId(int churchPersonId) throws IOException {
        List<GroupMember> groupMembers = findAllGroupMembers();
        List<String> departmentsInvolvedIn = new ArrayList<>();
        for (GroupMember groupMember : groupMembers) {
            if (groupMember.getPersonId() == churchPersonId) {
                departmentsInvolvedIn.add(getDepartmentByGroupId(groupMember.getGroupId()));
            }
        }

        while (true) {
            int departmentGroupSizeBeforeRemoving = departmentsInvolvedIn.size();
            deleteDuplicateFromDepartmentsInvolvedInList(departmentsInvolvedIn);
            if (departmentsInvolvedIn.size() == departmentGroupSizeBeforeRemoving) {
                break;
            } else {
                deleteDuplicateFromDepartmentsInvolvedInList(departmentsInvolvedIn);
            }
        }
        return departmentsInvolvedIn;
    }

    /**
     * delete duplicates from a list
     *
     * @param departments-
     */
    private void deleteDuplicateFromDepartmentsInvolvedInList(List<String> departments) {
        for (int i = 0; i < departments.size() - 1; i++) {
            String department = departments.get(i);
            for (int j = i + 1; j < departments.size(); j++) {
                if (department.equals(departments.get(j))) {
                    departments.remove(departments.get(j));
                }
            }
        }
    }

    public List<GroupMember> findAllGroupMembers() throws IOException {
        String fileContent = Files.readString(GROUPS);
        String[] lines = fileContent.split(Separator.END_LINE);

        List<String> roles = new ArrayList<>();
        List<GroupMember> memberRoles = new ArrayList<>();
        for (String line : lines) {
            String[] lineContent = line.split(Separator.COMMA);
            GroupMember groupMember = new GroupMember();
            for (int i = 2; i < lineContent.length; i++) {
                if (i == lineContent.length - 1) {
                    lineContent[i] = lineContent[i].trim();
                }
                roles.add(lineContent[i]);
            }
            groupMember.setPersonId(Integer.parseInt(lineContent[1].trim()));
            groupMember.setRoles(roles);
            groupMember.setGroupId(Integer.parseInt(lineContent[0]));
            memberRoles.add(groupMember);
            roles = new ArrayList<>();
        }
        return memberRoles;
    }

    public List<GroupMember> findGroupMembersByGroupId(int groupId) throws IOException {
        List<GroupMember> groupMembers = findAllGroupMembers();
        List<GroupMember> groupMembersRolesByName = new ArrayList<>();

        for (GroupMember groupMember : groupMembers) {
            if (groupMember.getGroupId() == groupId) {
                groupMembersRolesByName.add(groupMember);
            }
        }
        return groupMembersRolesByName;
    }

    @Override
    protected Path getPath() {
        return DEPARTMENTS_PATH;
    }

    @Override
    protected int generateId() {
        return ++departmentId;
    }

}
