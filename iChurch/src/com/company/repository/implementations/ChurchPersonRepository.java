package com.company.repository.implementations;

import com.company.repository.util.Separator;
import com.company.model.*;
import com.company.repository.GeneralRepository;
import com.company.repository.util.FileService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

import static com.company.repository.util.FilePath.*;

public class ChurchPersonRepository extends GeneralRepository<ChurchPerson> {

    private final DepartmentRepository departmentRepository;

    private final MemberRepository memberRepository;

    private final CaregiverRepository caregiverRepository;

    public ChurchPersonRepository(DepartmentRepository departmentRepository, MemberRepository memberRepository, CaregiverRepository caregiverRepository) throws IOException {
        super(ChurchPerson::new);
        this.departmentRepository = departmentRepository;
        this.memberRepository = memberRepository;
        this.caregiverRepository = caregiverRepository;
    }

    public void insertPersonInADepartmentGroup(GroupMember groupMember) throws IOException {
        List<GroupMember> groupMemberRoles = departmentRepository.findAllGroupMembers();
        groupMemberRoles.add(groupMember);
        FileService.deleteFileContent(GROUPS);
        Files.write(GROUPS, FileService.updateGroupsFile(groupMemberRoles).getBytes());
    }

    public void insertResponsibilities(ChurchPerson churchPerson) throws IOException {
        List<String> lines = Files.readAllLines(RESPONSIBILITIES_PATH);

        String fileReady = "";
        String churchPersonResponsibilitiesReady = "";
        boolean isInsert = false;
        List<String> churchPersonResponsibilities = churchPerson.getResponsibilityInChurch();
        for (String line : lines) {
            String[] csv = line.split(Separator.COMMA);
            if (Integer.parseInt(csv[0]) == (churchPerson.getId())) {
                for (int i = 0; i < churchPersonResponsibilities.size(); i++) {
                    if (i == churchPersonResponsibilities.size() - 1) {
                        churchPersonResponsibilitiesReady += churchPersonResponsibilities.get(i);
                    } else {
                        churchPersonResponsibilitiesReady += churchPersonResponsibilities.get(i) + ",";
                    }
                }
                fileReady += churchPerson.getId() + "," + churchPersonResponsibilitiesReady + "\n";
                isInsert = true;
            } else {
                fileReady += line + "\n";
            }
        }

        if (!isInsert) {
            Files.write(RESPONSIBILITIES_PATH, FileService.personResponsibilityReady(churchPerson).getBytes(), StandardOpenOption.APPEND);
        } else {
            FileService.deleteFileContent(RESPONSIBILITIES_PATH);
            Files.write(RESPONSIBILITIES_PATH, fileReady.getBytes());
        }
    }

    @Override
    protected Path getPath() {
        return null;
    }

    @Override
    protected int generateId() {
        return 0;
    }


    @Override
    public List<ChurchPerson> findAll() throws IOException {
        List<ChurchPerson> churchPeople = new ArrayList<>();
        churchPeople.addAll(memberRepository.findAll());
        churchPeople.addAll(caregiverRepository.findAll());
        return churchPeople;
    }

    @Override
    protected String entityListReady(List<ChurchPerson> entities) {
        String responsibilitiesFileReady = "";
        String responsibilitiesOfAChurchPersonReady = "";
        for (ChurchPerson churchPerson : entities) {
            List<String> responsibilitiesOfAChurchPerson = churchPerson.getResponsibilityInChurch();
            for (int i = 0; i < responsibilitiesOfAChurchPerson.size(); i++) {
                if (i == responsibilitiesOfAChurchPerson.size() - 1) {
                    responsibilitiesOfAChurchPersonReady += responsibilitiesOfAChurchPerson.get(i) + "\n";
                } else {
                    responsibilitiesOfAChurchPersonReady += responsibilitiesOfAChurchPerson.get(i) + ",";
                }
            }
            responsibilitiesFileReady += churchPerson.getId() + "," + responsibilitiesOfAChurchPersonReady;
            responsibilitiesOfAChurchPersonReady = "";
        }
        return responsibilitiesFileReady;
    }

}


