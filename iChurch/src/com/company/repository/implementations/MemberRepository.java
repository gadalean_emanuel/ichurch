package com.company.repository.implementations;

import com.company.repository.util.Separator;
import com.company.model.Member;
import com.company.repository.GeneralRepository;
import com.company.repository.util.Tools;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static com.company.repository.util.FilePath.MEMBERS_PATH;
import static com.company.repository.util.FilePath.RESPONSIBILITIES_PATH;

public class MemberRepository extends GeneralRepository<Member> {

    private int memberId;

    private final DepartmentRepository departmentRepository;

    public MemberRepository(DepartmentRepository departmentRepository) throws IOException {
        super(Member::new);
        this.departmentRepository = departmentRepository;
        memberId = startValue();
    }

    private int startValue() throws IOException {
        if (findAll().isEmpty()) {
            return 1;
        } else {
            return findAll().get(findAll().size() - 1).getId();
        }
    }

    private List<String> findResponsibilitiesByMemberId(int id) throws IOException {
        String fileContent = Files.readString(RESPONSIBILITIES_PATH);
        String[] lines = fileContent.split(Separator.END_LINE);

        List<String> responsibilities = new ArrayList<>();
        int memoPos = Tools.getPositionOfAnObjectInAStringArray(id, lines);
        if (memoPos > -1) {
            String[] responsibilitiesData = lines[memoPos].split(Separator.COMMA);
            for (int i = 1; i < responsibilitiesData.length; i++) {
                if (i == responsibilitiesData.length - 1) {
                    responsibilities.add(responsibilitiesData[i].trim());
                } else {
                    responsibilities.add(responsibilitiesData[i]);
                }
            }
        } else {
            return new ArrayList<>();
        }

        return responsibilities;
    }

    @Override
    public List<Member> findAll() throws IOException {
        List<Member> members = super.findAll();
        for (Member member : members) {
            member.setResponsibilityInChurch(findResponsibilitiesByMemberId(member.getId()));
            member.setDepartmentsInvolvedIn(departmentRepository.findDepartmentsNamesForChurchPersonId(member.getId()));
        }
        return members;
    }

    @Override
    protected Path getPath() {
        return MEMBERS_PATH;
    }

    @Override
    protected int generateId() {
        return ++memberId;
    }



}
