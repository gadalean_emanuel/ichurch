package com.company.repository.implementations;

import com.company.repository.util.Separator;
import com.company.model.Church;
import com.company.model.Floor;
import com.company.model.Hall;
import com.company.model.Router;
import com.company.repository.GeneralRepository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.company.repository.util.FilePath.*;

public class ChurchRepository extends GeneralRepository<Church> {

    public ChurchRepository() {
        super(Church::new);
    }

    public Church find() throws IOException {
        String fileContent = Files.readString(CHURCH_GENERAL_INFO);
        String[] lines = fileContent.split(Separator.END_LINE);

        String[] line = lines[0].split(Separator.COMMA);
        Church church = new Church(line[0], line[1], line[2]);
        church.setFloors(findAllFloors());
        return church;
    }

    private List<Floor> findAllFloors() throws IOException {
        String fileContent = Files.readString(FLOORS);
        String[] lines = fileContent.split(Separator.END_LINE);

        List<Floor> floors = new ArrayList<>();
        for (String line : lines) {
            String[] floorData = line.split(Separator.COMMA);
            Floor floor = new Floor(Integer.parseInt(floorData[0]), Integer.parseInt(floorData[1]), Integer.parseInt(floorData[2]), Integer.parseInt(floorData[3]));
            floors.add(floor);
        }
        return floors;
    }

    public Router findHallFloorRouter() throws IOException {
        String fileContent = Files.readString(FLOORS);
        String[] lines = fileContent.split(Separator.END_LINE);

        Map<Integer, List<Integer>> routerMap = new HashMap<>();
        int floorPos = 0;
        for (String line : lines) {
            String[] floorData = line.split(Separator.COMMA);
            List<Integer> hallsIds = new ArrayList<>();
            for (int i = 4; i < floorData.length; i++) {
                if (i == floorData.length - 1) {
                    hallsIds.add(Integer.parseInt(floorData[i].trim()));
                } else {
                    hallsIds.add(Integer.parseInt(floorData[i]));
                }
            }
            routerMap.put(findAllFloors().get(floorPos++).getId(), hallsIds);
        }

        Router router = new Router();
        router.setRouter(routerMap);
        return router;
    }

    public Hall findHallById(int id) throws IOException {
        String fileContent = Files.readString(HALLS);
        String[] lines = fileContent.split(Separator.END_LINE);

        String[] hallData = lines[id].split(Separator.COMMA);
        return new Hall(Integer.parseInt(hallData[0]), Integer.parseInt(hallData[1]), Integer.parseInt(hallData[2]), hallData[3]);
    }

    @Override
    protected Path getPath() {
        return CHURCH_GENERAL_INFO;
    }

    @Override
    protected int generateId() {
        return 0;
    }

    @Override
    public boolean insert(Church entity) throws IOException {
        return false;
    }

    @Override
    public Church findById(int id) {
        return null;
    }

    @Override
    public boolean delete(Church entity) {
        return false;
    }


}
