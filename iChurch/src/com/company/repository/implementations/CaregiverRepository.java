package com.company.repository.implementations;

import com.company.repository.util.Separator;
import com.company.model.Caregiver;
import com.company.model.GroupMember;
import com.company.model.Member;
import com.company.repository.GeneralRepository;
import com.company.repository.util.FileService;
import com.company.repository.util.Tools;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static com.company.repository.util.FilePath.*;

public class CaregiverRepository extends GeneralRepository<Caregiver> {

    private int caregiverId;

    private final DepartmentRepository departmentRepository;

    private final MemberRepository memberRepository;

    public CaregiverRepository(DepartmentRepository departmentRepository, MemberRepository memberRepository) throws IOException {
        super(Caregiver::new);
        this.departmentRepository = departmentRepository;
        this.memberRepository = memberRepository;
        caregiverId = startValue();
    }

    private int startValue() throws IOException {
        if (findAll().isEmpty()) {
            return 10000;
        } else {
            return findAll().get(findAll().size() - 1).getId();
        }
    }

    private List<String> getResponsibilitiesById(int id) throws IOException {
        String fileContent = Files.readString(RESPONSIBILITIES_PATH);
        String[] lines = fileContent.split(Separator.END_LINE);

        List<String> responsibilities = new ArrayList<>();
        int memoPos = Tools.getPositionOfAnObjectInAStringArray(id, lines);
        if (memoPos > -1) {
            String[] responsibilitiesData = lines[memoPos].split(Separator.COMMA);
            for (int i = 1; i < responsibilitiesData.length; i++) {
                if (i == responsibilitiesData.length - 1) {
                    responsibilities.add(responsibilitiesData[i].trim());
                } else {
                    responsibilities.add(responsibilitiesData[i]);
                }
            }
        } else {
            return new ArrayList<>();
        }

        return responsibilities;
    }


    public void fromCaregiverToMember(Caregiver caregiver, Member newMember) throws IOException {
        List<Caregiver> caregivers = findAll();
        caregivers.remove(caregiver);
        memberRepository.insert(newMember);

        FileService.deleteFileContent(CAREGIVERS_PATH);
        Files.write(CAREGIVERS_PATH, super.entityListReady(caregivers).getBytes());

        List<GroupMember> groupMembers = departmentRepository.findAllGroupMembers();
        List<Member> members = memberRepository.findAll();
        FileService.deleteFileContent(GROUPS);
        Files.write(GROUPS, FileService.updateIdInGroupFile(newMember.getId(), members, groupMembers).getBytes());
    }


    @Override
    public List<Caregiver> findAll() throws IOException {
        List<Caregiver> caregivers = super.findAll();
        for (Caregiver caregiver : caregivers) {
            caregiver.setDepartmentsInvolvedIn(departmentRepository.findDepartmentsNamesForChurchPersonId(caregiver.getId()));
            caregiver.setResponsibilityInChurch(getResponsibilitiesById(caregiver.getId()));
        }
        return caregivers;
    }

    @Override
    protected Path getPath() {
        return CAREGIVERS_PATH;
    }

    @Override
    protected int generateId() {
        return --caregiverId;
    }

}
