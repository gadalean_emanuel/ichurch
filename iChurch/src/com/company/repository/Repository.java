package com.company.repository;

import java.io.IOException;
import java.util.List;

///crud = create read update delete
public interface Repository<E> {

    boolean insert(E entity) throws IOException;

    E findById(int id) throws IOException;

    List<E> findAll() throws IOException;

    void update(E entity) throws IOException;

   boolean delete(E entity) throws IOException;

}
